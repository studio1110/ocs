PIXIScene.image("background", "img/OFC-App-V3-Elements-01.png", 180, 320);
PIXIScene.image("chapeau", "img/OFC-App-V3-Elements-02.png", 180, 350, 0);

PIXIScene.sound("tirlibibi", "resources/tirlibibi.mp3");
PIXIScene.sound("deviensmagicien", "resources/deviensmagicien.mp3");
PIXIScene.sound("clicclac", "resources/clicclac.mp3");

PIXIScene.button("bouton", "img/OFC-App-V3-Elements-04.png", 0, 0, 1);

PIXIScene.action("bouton", function (ev) {
    PIXIScene.asset("clicclac").play();
    PIXIScene.buttonAnimation("bouton", "demo");
});

PIXIScene.after(function (assets, game) {
    assets.tirlibibi.play();
    TweenMax.set(assets.chapeau, {pixi: {scale: 0.44}});

    PIXIScene.alignCenterMiddle(assets.chapeau,null,{y:-20});
    PIXIScene.alignCenterBottom(assets.bouton, assets.chapeau, {x:580,y: 140});

    _.delay(function () {
        TweenMax.to(assets.chapeau, 1, {pixi: {alpha: 1, scale: 0.5}, ease: Elastic.easeOut.config(1, 0.3)});
        TweenMax.to(PIXIScene.asset("bouton"), 1, {pixi: {x: 180, alpha: 1}, delay: 1});
        TweenMax.set(PIXIScene.asset("chapeau"), {pixi: {scale: 0.43}});
    }, 3000)

});
