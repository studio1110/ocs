/**
 * Automatically generated file. DO NOT MODIFY
 */
package boomiz.onzedix.fr;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "boomiz.onzedix.fr";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 15500;
  public static final String VERSION_NAME = "1.55.0";
}
